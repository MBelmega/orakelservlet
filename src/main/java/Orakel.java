import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;

/**
 * Created by majab on 05.09.2017.
 */
public class Orakel extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String frage = req.getParameter("frage");
        req.setAttribute("frage", frage);
        String antwort = generateAnswer();
        req.setAttribute("antwort", antwort);

        RequestDispatcher dispatcher = req.getRequestDispatcher("/antwort.jsp");

        dispatcher.forward(req, resp);


    }

    private String generateAnswer() {
        switch(new Random().nextInt(5)){
            case 0: return"Dies ist eine gefährliche Frage.";
            case 1: return "Die Zeichen sprechen dagegen.";
            case 2: return "Das Glück ist mit den Tapfewren!";
            case 3: return "Schau nach vorn!";
            default: return "Geduld du musst lernen, junger Padawan!";
        }
    }
}